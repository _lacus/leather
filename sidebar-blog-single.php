<?php
/**
 * Single post page sidebar template
 *
 * PHP version 7
 *
 * @category   Sidebar_Template
 * @package    WordPress
 * @subpackage Leather
 * @author     Varga László <varga.laszlo@protonmail.com>
 * @license    GNU General Public License v2 or later
 * @link       https://developer.wordpress.org/themes/functionality/sidebars/
 * @since      Leather 1.0
 */
?>
<aside class="
    col 
    col-12
    col-sm-4
    col-md-3
    col-lg-3
    order-last
    order-sm-first">
    <h2 class="sr-only">
        <?php 
            __('More from', 'leather');
            echo ' '; 
            bloginfo('name'); ?>
    </h2>
    <?php if (is_active_sidebar('blog_single_left') ) : ?>
        <div 
            id="primary-sidebar" 
            class="primary-sidebar widget-area" 
            role="complementary">
            <?php dynamic_sidebar('blog_single_left'); ?>
        </div>
    <?php endif; ?>
</aside>