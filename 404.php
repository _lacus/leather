<?php
/**
 * 404 page template file
 *
 * PHP version 7
 *
 * @category   Page_Template
 * @package    WordPress
 * @subpackage Leather
 * @author     Varga László <varga.laszlo@protonmail.com>
 * @license    GNU General Public License v2 or later
 * @link       https://codex.wordpress.org/Creating_an_Error_404_Page
 * @since      Leather 1.0
 */
 
get_header(); ?>
    <div class="container">
        <div class="breadcrumbs">
            <?php if (function_exists('bcn_display')) :
                bcn_display();
            endif; ?>
        </div>
        <main id="main" class="col">
            <article class="single-page">
                <div class="text-center">
                    <h2>
                        <?php _e('Error: 404 - Page not found', 'leather'); ?>
                    </h2>
                    <h3>
                        <?php _e(
                            'The page you requested does not exist',
                            'leather'
                        ); ?>
                    </h3>
                    <br>
                    <i class="fa fa-chain-broken fa-5x" aria-hidden="true"></i>
                    <br>
                    <br>
                    <a href="/" class="btn btn-default btn-lg">
                        <?php _e('Go to Homepage', 'leather'); ?> 
                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                    </a>
                </div>
            </article>
        </main>
    </div>
<?php get_footer(); ?>
