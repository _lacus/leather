<?php
/**
 * Leather theme functions file
 *
 * PHP version 7
 *
 * @category   Functions
 * @package    WordPress
 * @subpackage Leather
 * @author     Varga László <varga.laszlo@protonmail.com>
 * @license    GNU General Public License v2 or later
 * @link       https://codex.wordpress.org/Functions_File_Explained
 * @since      Leather 1.0
 */

/**
 * WordPress add theme support
 */

// Title tag support
add_theme_support('title-tag');

// Post thumbnails
add_theme_support('post-thumbnails');

// Custom background
$defaults = array(
    'default-color'          => '#ffffff',
    'default-repeat'         => 'no-repeat',
    'default-position-x'     => 'center',
    'default-position-y'     => 'center',
    'wp-head-callback'       => '_custom_background_cb'
);
add_theme_support('custom-background', $defaults);

// RSS
add_theme_support('automatic-feed-links');

// Custom header image
$args = array(
	'flex-width'        => true,
	'width'             => 1140,
	'flex-height'       => true,
	'height'            => 200,
    'random-default'    => false,
);
add_theme_support( 'custom-header', $args );

/**
 * Custom logo setup
 */
function themename_custom_logo_setup() {
    $defaults = array(
        'height'      => 70,
        'flex-height' => false,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    );
    add_theme_support( 'custom-logo', $defaults );
}
add_action( 'after_setup_theme', 'themename_custom_logo_setup' );

// Images
if (! isset($content_width) ) {
    $content_width = 1140;
}

if (function_exists('add_image_size') ) {
    add_image_size('bloglist', 845, 845);
}

/**
 * Load lang files from theme folder
*/
load_theme_textdomain( 'leather', get_template_directory().'/languages' );

/**
* Register CSS and JS files
*
* @return null
*/
function add_theme_scripts() 
{
    $template_uri = get_template_directory_uri();

    wp_enqueue_style('app', $template_uri . '/style.css', array(), '', 'all');

    wp_enqueue_script('bootstrap-js', $template_uri . '>/js/bootstrap.min.js', array ( 'jquery' ), '', true);
    //wp_enqueue_script('script', $template_uri . '/js/script.js', array ( 'jquery' ), '', true);

    if (is_search()) {
        wp_enqueue_script('mark', $template_uri . '/js/jquery.mark.min.js', array ( 'jquery' ), '', true);
        wp_enqueue_script('search', $template_uri . '/js/search.js', array ( 'jquery' ), '', true);
    }
}
add_action('wp_enqueue_scripts', 'Add_Theme_scripts');

// Editor style
add_editor_style( 'editor-style.css' );

// Register Menus
register_nav_menu('primary', __('Main menu', 'leather'));
register_nav_menu('footer', __('Footer menu', 'leather'));
register_nav_menu('footer-social', __('Footer social menu', 'leather'));

// Comments 
add_filter('comment_form_default_fields', 'bootstrap3_comment_form_fields');
/**
 * Make Comment form fields bootstrap compatible
 *
 * @return none
 */
function bootstrap3_comment_form_fields( $fields ) 
{
    $commenter = wp_get_current_commenter();
    
    $req      = get_option('require_name_email');
    $aria_req = ( $req ? " aria-required='true'" : '' );
    $html5    = current_theme_supports('html5', 'comment-form') ? 1 : 0;
    
    $fields   =  array(
        'author' => '<div class="form-group comment-form-author">' . '<label for="author">' . __('Name', 'leather') . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
                    '<input class="form-control" id="author" name="author" type="text" value="' . esc_attr($commenter['comment_author']) . '" size="30"' . $aria_req . ' /></div>',
        'email'  => '<div class="form-group comment-form-email"><label for="email">' . __('Email', 'leather') . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
                    '<input class="form-control" id="email" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr($commenter['comment_author_email']) . '" size="30"' . $aria_req . ' /></div>',
        'url'    => '<div class="form-group comment-form-url"><label for="url">' . __('Website', 'leather') . '</label> ' .
                    '<input class="form-control" id="url" name="url" ' . ( $html5 ? 'type="url"' : 'type="text"' ) . ' value="' . esc_attr($commenter['comment_author_url']) . '" size="30" /></div>'        
    );
    
    return $fields;
}

add_filter('comment_form_defaults', 'bootstrap3_comment_form');
/**
 * Make Comment fields bootstrap compatible
 *
 * @return none
 */
function bootstrap3_comment_form( $args ) 
{
    $args['comment_field'] = '<div class="form-group comment-form-comment">
            <label for="comment">' . __('Comment', 'leather') . '</label> 
            <textarea class="form-control" id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea>
        </div>';
    $args['class_submit'] = 'btn btn-default';
    
    return $args;
}

/**
 * Init widgets
 *
 * @return none
 */
function leather_widgets_init() 
{
    register_sidebar(
        array(
        'name'          => __('Blog left sidebar', 'leather'),
        'id'            => 'blog_left',
        'before_widget' => '<section id="%1$s" class="%2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>',
        ) 
    );
    register_sidebar(
        array(
        'name'          => __('Blog left sidebar - single blogpost', 'leather'),
        'id'            => 'blog_single_left',
        'before_widget' => '<section id="%1$s" class="%2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>',
        ) 
    );
    register_sidebar(
        array(
        'name'          => __('Page left sidebar - single page', 'leather'),
        'id'            => 'page_single_left',
        'before_widget' => '<section id="%1$s" class="%2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>',
        ) 
    );
}
add_action('widgets_init', 'leather_widgets_init');

add_filter('widget_tag_cloud_args', 'set_tag_cloud_font_size');
/**
 * This function sets the tag clouds font size
 *
 * @return meeh
 */
function set_tag_cloud_font_size($args) 
{
    $args['smallest'] = 7;
    $args['largest'] = 17;
    return $args; 
}

/**
 * Tags as meta keywords
 *
 * @return taglist
 */ 
function tags_as_meta() 
{
    $tags = get_tags();
    foreach ( $tags as $tag ) {    
        $tag_list .= "{$tag->name}";
        $tag_list .= ', ';
    }
    return $tag_list;
}

/**
 * Third party tools
 */

// TMG plugin activation
require_once get_template_directory() . '/lib/tgm_plugin_activation/class-tgm-plugin-activation.php';
require_once get_template_directory() . '/lib/tgm_plugin_activation/required-plugin-settings.php';
add_action('tgmpa_register', 'plugin_install_register_required_plugins');

// Bootstrap menu
require_once 'lib/wp_bootstrap_navwalker.php';

/**
 * Plugin related
 */

//Contact form 7
add_filter('wpcf7_load_js', '__return_false');
add_filter('wpcf7_load_css', '__return_false');

/**
 * Require activation script
 */
if (isset($_GET['activated']) && is_admin()){
    require_once 'inc/activation.php';
}

/**
 * Require theme settings
 */
require_once 'inc/theme_settings.php';
?>
