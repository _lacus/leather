<?php
/**
 * Footer template part
 *
 * PHP version 7
 *
 * @category   Template_Part
 * @package    WordPress
 * @subpackage Leather
 * @author     Varga László <varga.laszlo@protonmail.com>
 * @license    GNU General Public License v2 or later
 * @link       https://codex.wordpress.org/Theme_Development#Footer_.28footer.php.29
 * @since      Leather 1.0
 */
?>
<footer id="footer">
  <div class="container">  
    <div class="row">
      <div class="col col-xs-12 col-sm-12 col-md-7 col-lg-6">
        <h2>
          <a href="<?php echo esc_url(home_url()); ?>">
            <?php bloginfo('name'); ?>
          </a>
        </h2>
      </div>
      <div class="col col-xs-12 col-sm-12 col-md-5 col-lg-6">
          <nav id="footer_social_menu">
            <h3 class="sr-only">
                <?php bloginfo('name'); ?> 
                <?php _e('on social networks', 'leather'); ?>
            </h3>
            <?php wp_nav_menu(
                array(
                  'theme_location' => 'footer-social',
                  'menu_class' => 'nav-menu' 
                )
            ); ?>
          </nav>
          <nav id="footer_menu">
            <h3 class="sr-only">
                <?php bloginfo('name'); ?> <?php _e('subpages', 'leather'); ?>
            </h3>
            <?php wp_nav_menu(
                array( 'theme_location' => 'footer', 'menu_class' => 'nav-menu' )
            ); ?>
          </nav>
      </div>
      <div class="col col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <p id="copy_text" class="text-center">
          &copy; Copyright - <?php bloginfo('name'); ?> <?php echo date("Y"); ?>
        </p>
      </div>
    </div>
  </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
