# Contributing on Leather theme

## Theme development

### To compile CSS from SCSS

#### Install [Ruby](https://www.ruby-lang.org/en/)  

[Windows installer](https://rubyinstaller.org/)  
  
Linux installation via command line:

```bash
  sudo apt-get install ruby
```

####  Install SASS, Compass

In command line type:

```bash
  gem install sass compass
```
#### Compile

From theme root folder in command line type:

```bash
  compass compile
```
Or to watch files:

```bash
  compass watch
```

### File structure
```
wp-content/themes/leather   # Theme root
├── components              # Template parts folder
├── fonts                   # fonts folder
├── img                     # Image folder
├── inc                     # Includes
├── js                      # JS folder
├── languages               # Language files
├── lib                     # Third party libraries
├── SCSS                    # Sass files
│   ├── components          # Sass coponent parts
│   ├── library             # Third party components 
│   ├── sections            # Sass main components
```

### Resources

* [SASS](http://sass-lang.com/)
* [Compass](http://compass-style.org/)
