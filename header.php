<?php
/**
 * Header template file
 *
 * PHP version 7
 *
 * @category   Template_Part
 * @package    WordPress
 * @subpackage Leather
 * @author     Varga László <varga.laszlo@protonmail.com>
 * @license    GNU General Public License v2 or later
 * @link       https://developer.wordpress.org/themes/basics/template-files/#template-partials
 * @since      Leather 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> <?php echo get_option("color_settings") ?  'class="color-' . get_option("color_settings") . '"' : "color-green"; ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <link rel="icon" type="image/png" href="<?php echo esc_url(get_template_directory_uri()); ?>/img/favicon.png" />
    <title><?php wp_title(); ?></title>
    <meta name="description" content="<?php wp_title('|', true, 'right'); ?><?php bloginfo('name'); ?>">
    <meta name="author" content="_lacus">
    <meta name="keywords" content="<?php print tags_as_meta() ?>">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <?php if (is_page('kapcsolat')) :
        if (function_exists('wpcf7_enqueue_scripts')) :
               wpcf7_enqueue_scripts();
        endif;
        if (function_exists('wpcf7_enqueue_styles')) :
               wpcf7_enqueue_styles();
        endif;
    endif; ?>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <h1 class="sr-only"><?php bloginfo('name'); ?></h1>
    <ul class="sr-only">
        <li><a href="/#main"><?php _e('Go to content', 'leather'); ?></a></li>
        <li><a href="/#search"><?php _e('Go to search', 'leather'); ?></a></li>
    </ul>
    <header>
        <nav class="navbar navbar-dark navbar-expand-md justify-content-between">
            <h2 class="sr-only"><?php _e('Main menu', 'leather'); ?></h2> 
            <div class="container">
                <?php 
                $custom_logo_id = get_theme_mod( 'custom_logo' );
                $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                if ( has_custom_logo() ) {
                    echo '<a class="navbar-brand brand-image" href="' . esc_url(home_url()) . '"><img src="'. esc_url( $logo[0] ) .'"></a>';
                } else {
                    echo '<a class="navbar-brand" href="' . esc_url(home_url()) . '">'. get_bloginfo( 'name' ) .'</a>';
                } ?>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-main" aria-controls="navbar-main" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbar-main">
                    <?php
                    $menuMain = "primary";
                    wp_nav_menu(
                        array(
                            'theme_location' => $menuMain,
                            'depth' => 2,
                            'container' => false,
                            'menu_class' => 'navbar-nav mr-auto',
                            'walker' => new wp_bootstrap_navwalker()
                        )
                    );
                    get_search_form();
                    ?>
                </div>
            </div>
        </nav>
        <?php if(get_header_image() == true) : ?>
            <div 
                class="custom-header-image" 
                style="background-image: url('<?php header_image(); ?>'); min-height:  <?php echo get_custom_header()->height; ?>px;"
            ></div>
        <?php endif; ?>
    </header>
