<?php
/**
 * Settings for TGM plugin activation
 *
 * PHP version 7
 *
 * @category   Sidebar_Template
 * @package    WordPress
 * @subpackage Leather
 * @author     Varga László <varga.laszlo@protonmail.com>
 * @license    GNU General Public License v2 or later
 * @link       http://tgmpluginactivation.com/
 * @since      Leather 1.0
 */

?>
<?php
/**
* Automatic plugin installation settings
*
* @since   Leather 1.0
* @package Leather
* @author  Varga László
* @return  Init
*/
function plugin_install_register_required_plugins()
{

    // Plugins
    $plugins = array(

        array(
            'name'      => 'Advanced Custom Fields',
            'slug'      => 'advanced-custom-fields',
            'required'  => false
        ),
            
        array(
            'name'      => 'Automatic Featured Images from Videos',
            'slug'      => 'automatic-featured-images-from-videos',
            'required'  => false
        ),

        array(
            'name'      => 'Breadcrumb NavXT',
            'slug'      => 'breadcrumb-navxt',
            'required'  => true
        ),

        array(
            'name'      => 'Broken Link Checker',
            'slug'      => 'broken-link-checker',
            'required'  => false
        ),

        array(
            'name'      => 'Bootstrap for Contact Form 7',
            'slug'      => 'bootstrap-for-contact-form-7',
            'required'  => false
        ),

        array(
            'name'      => 'Contact Form 7',
            'slug'      => 'contact-form-7',
            'required'  => false
        ),

        array(
            'name'      => 'Contact Form DB',
            'slug'      => 'contact-form-7-to-database-extension',
            'required'  => false
        ),

        array(
            'name'      => 'Crayon Syntax Highlighter',
            'slug'      => 'crayon-syntax-highlighter',
            'required'  => false
        ),

        array(
            'name'      => 'Custom Post Type UI',
            'slug'      => 'custom-post-type-ui',
            'required'  => false
        ),

        array(
            'name'      => 'Custom Post Type Widgets',
            'slug'      => 'custom-post-type-widgets',
            'required'  => false
        ),

        array(
            'name'      => 'Delete Post Revisions In WordPress',
            'slug'      => 'delete-post-revisions-on-single-click',
            'required'  => false
        ),

        array(
            'name'      => 'Google Analyticator',
            'slug'      => 'google-analyticator',
            'required'  => false
        ),

        array(
            'name'      => 'Google XML Sitemaps',
            'slug'      => 'google-sitemap-generator',
            'required'  => false
        ),

        array(
            'name'      => 'Login LockDown',
            'slug'      => 'login-lockdown',
            'required'  => false
        ),

        array(
            'name'      => 'Remove Wordpress Overhead',
            'slug'      => 'remove-wordpress-overhead',
            'required'  => false
        ),

        array(
            'name'      => 'Shortcode Widget',
            'slug'      => 'shortcode-widget',
            'required'  => false
        ),

        array(
            'name'      => 'Tablepress',
            'slug'      => 'tablepress',
            'required'  => false
        ),

        array(
            'name'      => 'Widget CSS Classes',
            'slug'      => 'widget-css-classes',
            'required'  => true
        ),

        array(
            'name'      => 'WP-DB-Backup',
            'slug'      => 'wp-db-backup',
            'required'  => false
        ),

        array(
            'name'      => 'WP Meta SEO',
            'slug'      => 'wp-meta-seo',
            'required'  => false
        ),

        array(
            'name'      => 'WP Page Numbers',
            'slug'      => 'wp-page-numbers',
            'required'  => true
        ),

        array(
            'name'      => 'WP reCaptcha Integration',
            'slug'      => 'wp-recaptcha-integration',
            'required'  => false
        ),

        array(
            'name'      => 'WP Super Cache',
            'slug'      => 'wp-super-cache',
            'required'  => false
        ),

        array(
            'name'      => 'Widget Content Blocks',
            'slug'      => 'wysiwyg-widgets',
            'required'  => false
        ),
    );

    // Configuration
    $config = array(
        'id'           => 'leather',
        'default_path' => '',
        'menu'         => 'tgmpa-install-plugins',
        'parent_slug'  => 'themes.php',
        'capability'   => 'edit_theme_options',
        'has_notices'  => true,
        'dismissable'  => true,
        'dismiss_msg'  => '',
        'is_automatic' => false,
        'message'      => '',
    );

    // Init
    tgmpa($plugins, $config);
}
