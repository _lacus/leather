<?php
/**
 * Search page template
 *
 * PHP version 7
 *
 * @category   Page_Template
 * @package    WordPress
 * @subpackage Leather
 * @author     Varga László <varga.laszlo@protonmail.com>
 * @license    GNU General Public License v2 or later
 * @link       https://codex.wordpress.org/Creating_a_Search_Page
 * @since      Leather 1.0
 */

get_header(); ?>
<div class="container">
    <div class="row">
        <main id="main" class="
            herd col 
            col-12 
            col-sm-8 
            col-md-9 
            col-lg-9">
            <h2 class="search-title main_title">
                <?php echo $wp_query->found_posts; ?>
                <?php _e('search results for keyword', 'leather'); ?>
                <i>&ldquor;<span class="keyword"><?php the_search_query(); ?></span>&rdquor;</i>
            </h2>
            <?php if (have_posts() ) :
                while ( have_posts() ) : the_post();
                    get_template_part('components/search');
                endwhile;
                get_template_part('components/pagination');
            endif; ?>    
        </main>
    <?php get_sidebar('blog'); ?>
    </div>
</div>
<?php get_footer(); ?>