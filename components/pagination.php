<?php
/**
 * Pagination template part
 *  
 * Uses wp_page_numbers plugin if active
 *
 * PHP version 7
 *
 * @category   Template_Part
 * @package    WordPress
 * @subpackage Leather
 * @author     Varga László <varga.laszlo@protonmail.com>
 * @license    GNU General Public License v2 or later
 * @link       https://developer.wordpress.org/reference/functions/get_template_part/
 * @since      Leather 1.0
 */

if(function_exists('wp_page_numbers')) :
    wp_page_numbers(); 
else :
    echo '<div class="posts-nav-link">';
    posts_nav_link(
        ' ',
        '<i class="fa fa-chevron-left"></i> ' . __('Previous page', 'leather'),
        __('Next page', 'leather') . ' <i class="fa fa-chevron-right"></i>'
    );
    echo '</div>';
endif;
?>