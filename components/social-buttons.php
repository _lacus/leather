<?php
/**
 * Social buttons template part
 *
 * PHP version 7
 *
 * @category   Template_Part
 * @package    WordPress
 * @subpackage Leather
 * @author     Varga László <varga.laszlo@protonmail.com>
 * @license    GNU General Public License v2 or later
 * @link       https://developer.wordpress.org/reference/functions/get_template_part/
 * @since      Leather 1.0
 */

if (get_option('facebook_app_id')) {
$app_id = get_option('facebook_app_id');
$app_lang = get_locale();
$app_url = "//connect.facebook.net/" . $app_lang . "/sdk.js#xfbml=1&version=v2.8&appId=" . $app_id;
?>
    <hr>
    <div class="social-share">
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "<?php echo $app_url; ?>";
        fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        <div 
            class="fb-like" 
            data-href="<?php the_permalink(); ?>" 
            data-layout="button_count" 
            data-action="like" 
            data-size="small" 
            data-show-faces="false" 
            data-share="true">
        </div>
    </div>
<?php
} ?>