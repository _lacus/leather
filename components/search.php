<?php
/**
 * Post template part on serach page
 *
 * PHP version 7
 *
 * @category   Template_Part
 * @package    WordPress
 * @subpackage Leather
 * @author     Varga László <varga.laszlo@protonmail.com>
 * @license    GNU General Public License v2 or later
 * @link       https://developer.wordpress.org/reference/functions/get_template_part/
 * @since      Leather 1.0
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="post-inner clearfix">
        <h3 class="search-result-title">
            <a href="<?php the_permalink(); ?>">
                <?php the_title(); ?>
            </a>
        </h3>
        <p class="meta">
            <?php echo __('Author', 'leather') . ": " . get_the_author() . " | " . get_the_date(); ?>
        </p>
        <?php echo get_the_tag_list(
            '<p class="tags"><span class="sr-only">' . 
            __('Tags', 'leather') .
            ': </span><i class="fa fa-tags" aria-hidden="true"></i> ',
            ', ', 
            '</p>'
        ); ?>
        <div class="search-result-content">
            <?php
                global $more;
                $more = 0;
                the_content(__('Read more', 'leather')); ?>
        </div>
        <br>
    </div>
</article>