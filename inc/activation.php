<?php
/**
 * Activation script
 *
 * Will create pages and menus on theme activation
 *
 * PHP version 7
 *
 * @category   Functions
 * @package    WordPress
 * @subpackage Leather
 * @author     Varga László <varga.laszlo@protonmail.com>
 * @license    GNU General Public License v2 or later
 * @link       https://codex.wordpress.org/Functions_File_Explained
 * @since      Leather 1.0
 */

?>