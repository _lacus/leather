<?php
/**
 * Theme settings
 *
 * PHP version 7
 *
 * @category   Functions
 * @package    WordPress
 * @subpackage Leather
 * @author     Varga László <varga.laszlo@protonmail.com>
 * @license    GNU General Public License v2 or later
 * @link       https://codex.wordpress.org/Functions_File_Explained
 * @since      Leather 1.0
 */

/**
 * Facebook settings
 */
function display_facebook_element()
{
	?>
    	<input type="text" name="facebook_app_id" id="facebook_app_id" value="<?php echo get_option('facebook_app_id'); ?>" />
    <?php
}

/**
 * Theme color settings
 */
function display_color_settings_element()
{
	?>
		<select name="color_settings" id="color_settings">
			<option value="" <?php echo get_option('color_settings') == "" ? "selected" : ""; ?>>Select color</option>
			<option value="gray" <?php echo get_option('color_settings') == "gray" ? "selected" : ""; ?>>GRAY</option>
			<option value="red"	<?php echo get_option('color_settings') == "red" ? "selected" : "";  ?>>RED</option>
			<option value="pink" <?php echo get_option('color_settings') == "pink" ? "selected" : "" ; ?>>PINK</option>
			<option value="grape" <?php echo get_option('color_settings') == "grape" ? "selected" : "";  ?>>GRAPE</option>
			<option value="violet" <?php echo get_option('color_settings') == "violet" ? "selected" : ""; ?>>VIOLET</option>
			<option value="indigo" <?php echo get_option('color_settings') == "indigo" ? "selected" : "";  ?>>INDIGO</option>
			<option value="blue" <?php echo get_option('color_settings') == "blue" ? "selected" : "" ; ?>>BLUE</option>
			<option value="cyan" <?php echo get_option('color_settings') == "cyan" ? "selected" : ""; ?>>CYAN</option>
			<option value="teal" <?php echo get_option('color_settings') == "teal" ? "selected" : ""; ?>>TEAL</option>
			<option value="green" <?php echo get_option('color_settings') == "green" ? "selected" : "";  ?>>GREEN</option>
			<option value="lime" <?php echo get_option('color_settings') == "lime" ? "selected" : ""; ?>>LIME</option>
			<option value="yellow" <?php echo get_option('color_settings') == "yellow" ? "selected" : "";  ?>>YELLOW</option>
			<option value="orange" <?php echo get_option('color_settings') == "orange" ? "selected" : ""; ?>>ORANGE</option>
		</select>
    <?php
}

/**
 * Theme settings fields
 */
function display_theme_panel_fields()
{
	add_settings_section("section", '<span class="dashicons dashicons-megaphone"></span> ' . __('Leather settings', 'leather'), null, "theme-options");
	
    add_settings_field("facebook_app_id", __('Facebook App Id', 'leather'), "display_facebook_element", "theme-options", "section");

	register_setting("section", "facebook_app_id");


	add_settings_field("color_settings", __('Color Settings', 'leather'), "display_color_settings_element", "theme-options", "section");
	
	register_setting("section", "color_settings");
}

add_action("admin_init", "display_theme_panel_fields");


/**
 * Creates theme settings page
 */
function theme_settings_page()
{
    ?>
	    <div class="wrap">
	    <h1><?php _e('Leather settings', 'leather'); ?></h1>
	    <form method="post" action="options.php">
	        <?php
	            settings_fields("section");
	            do_settings_sections("theme-options");      
	            submit_button(); 
	        ?>          
	    </form>
		</div>
	<?php
}

/**
 * Creates theme settings menu
 */
function add_theme_menu_item()
{
	 add_theme_page(
		$menu_title = __('Leather settings', 'leather'), 
		$page_title = __('Leather settings', 'leather'),
		$capability = "manage_options", 
		$menu_slug = "theme-panel", 
		$function = "theme_settings_page", 
		$icon_url = "dashicons-carrot", 
		$position = 99 
	);
}

add_action("admin_menu", "add_theme_menu_item");

?>