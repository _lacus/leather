<?php
/**
 * Single post page template
 *
 * PHP version 7
 *
 * @category   Page_Template
 * @package    WordPress
 * @subpackage Leather
 * @author     Varga László <varga.laszlo@protonmail.com>
 * @license    GNU General Public License v2 or later
 * @link       https://codex.wordpress.org/Theme_Development#Single_Post_.28single.php.29
 * @since      Leather 1.0
 */
 
get_header(); ?>
<div class="container">
    <div class="row">
        <main id="main" class="
            herd col 
            col-12 
            col-sm-8 
            col-md-9 
            col-lg-9">
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <h2><?php the_title(); ?></h2>
                    <p class="meta">
                        <?php echo __('Author', 'leather') . ": " . get_the_author() . " | " . get_the_date(); ?>
                    </p>
                    <?php echo get_the_tag_list(
                        '<p class="tags"><span class="sr-only">' . 
                        __('Tags', 'leather') .
                        ': </span><i class="fa fa-tags" aria-hidden="true"></i> ',
                        ', ', 
                        '</p>'
                    ); ?>
                    <?php 
                        the_content();
                        $defaults = array(
                            'before'           => '<nav class="link-pages">' . __( 'Pages:', 'leather' ),
                            'after'            => '</nav>',
                            'link_before'      => '',
                            'link_after'       => '',
                            'next_or_number'   => 'number',
                            'separator'        => ' ',
                            'pagelink'         => '%',
                            'echo'             => 1
                        );
                        wp_link_pages( $defaults );
                    ?>
                    <?php get_template_part('components/social-buttons'); ?>
                    <hr>
                    <?php 
                    if (comments_open() || get_comments_number() ) :
                        comments_template();
                    endif; ?>
                <?php endwhile; 
                endif; ?>
            </article>
        </main>
        <?php get_sidebar('blog-single'); ?>
    </div>
</div>
<?php get_footer(); ?>

