<?php
/**
 * Searchform template
 *
 * PHP version 7
 *
 * @category   Template_Part
 * @package    WordPress
 * @subpackage Leather
 * @author     Varga László <varga.laszlo@protonmail.com>
 * @license    GNU General Public License v2 or later
 * @link       https://codex.wordpress.org/Creating_a_Search_Page
 * @since      Leather 1.0
 */
?>

<form class="searchform form-inline my-2 my-lg-0">
    <label id="search" for="s" class="sr-only">
        <?php _e('Search', 'leather'); ?>
    </label>
    <div class="input-group">
        <input 
            class="form-control"
            type="search"
            name="s"
            id="s"
            placeholder="<?php _e('Search', 'leather'); ?>"
            value="<?php echo get_search_query(); ?>"
            aria-label="Search"
            required
        />
        <div class="input-group-append">
            <button 
                class="btn"
                type="submit" 
                id="searchsubmit" 
                title="<?php _e('Search', 'leather'); ?>
            ">
                <i class="fa fa-search" aria-hidden="true"></i>
                <span class="sr-only"> <?php _e('Search', 'leather'); ?>!</span>
            </button>
        </div>
    </div>
</form>