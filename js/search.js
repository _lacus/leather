window.onload = function () {
	if (window.jQuery) {
		jQuery(function ($) {
			var keyword = jQuery('.keyword').text();
			var $toMark = jQuery(".search-result-title a, .search-result-content, .search-result-content *", ".search-results main");
			$toMark.mark(keyword);
		});
	}
}