<?php
/**
 * Comments template
 *
 * PHP version 7
 *
 * @category   Template_Part
 * @package    WordPress
 * @subpackage Leather
 * @author     Varga László <varga.laszlo@protonmail.com>
 * @license    GNU General Public License v2 or later
 * @link       https://developer.wordpress.org/themes/template-files-section/partial-and-miscellaneous-template-files/comments/
 * @since      Leather 1.0
 */
?>
<?php
if (post_password_required() ) {
    return;
}
?>
<div id="comments" class="comments-area">
    <?php if (have_comments() ) : ?>
        <h2 class="comments-title">
            <i class="fa fa-comments-o" aria-hidden="true"></i>
            <?php comments_number(); ?>
        </h2>
    <?php the_comments_navigation(); ?>

        <ol class="comment-list">
    <?php
                wp_list_comments(
                    array(
                    'style'       => 'ol',
                    'short_ping'  => true,
                    'avatar_size' => 42,
                    ) 
                );
    ?>
        </ol>

    <?php the_comments_navigation(); ?>

    <?php endif; ?>

    <?php
    if (! comments_open() 
        && get_comments_number() 
        && post_type_supports(get_post_type(), 'comments') 
    ) :
        echo '<p class="no-comments">';
        _e('Comments are closed.', 'leather');
        echo '</p>';
    endif; ?>

    <?php
    comment_form(
        array(
        'title_reply_before' => '<h2 id="reply-title" class="comment-reply-title">',
        'title_reply_after'  => '</h2>',
        ) 
    );
    ?>
</div>
