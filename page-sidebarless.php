<?php
/**
 * Single page without sidebar template
 *
 * PHP version 7
 *
 * @category   Page_Template
 * @package    WordPress
 * @subpackage Leather
 * @author     Varga László <varga.laszlo@protonmail.com>
 * @license    GNU General Public License v2 or later
 * @link       https://developer.wordpress.org/themes/template-files-section/page-template-files/
 * @since      Leather 1.0
 */
 
 /* Template Name: Sidebarless page */
 get_header(); ?>
    <div class="container">
        <div class="breadcrumbs">
            <?php 
            if(function_exists('bcn_display')) : 
                bcn_display();
            endif;
            ?>
        </div>
        <div class="row">
            <main id="main" class="col col-12">
                <article class="single-page">
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <h2><?php the_title(); ?></h2>
                    <?php the_content(); ?>
                    <?php endwhile; 
                    endif; ?>
                </article>
            </main>
        </div>
    </div>
<?php get_footer(); ?>

