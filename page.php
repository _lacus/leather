<?php
/**
 * Single page template
 *
 * PHP version 7
 *
 * @category   Page_Template
 * @package    WordPress
 * @subpackage Leather
 * @author     Varga László <varga.laszlo@protonmail.com>
 * @license    GNU General Public License v2 or later
 * @link       https://developer.wordpress.org/themes/template-files-section/page-template-files/
 * @since      Leather 1.0
 */
 
 get_header(); ?>
    <div class="container">
        <div class="breadcrumbs">
            <?php 
            if(function_exists('bcn_display')) : 
                bcn_display();
            endif;
            ?>
        </div>
        <div class="row">
            <main id="main" class="
                herd col 
                col-12 
                col-sm-8 
                col-md-9 
                col-lg-9
            ">
                <article class="single-page">
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <h2><?php the_title(); ?></h2>
                    <?php 
                        the_content();
                        $defaults = array(
                            'before'           => '<nav class="link-pages">' . __( 'Pages:', 'leather' ),
                            'after'            => '</nav>',
                            'link_before'      => '',
                            'link_after'       => '',
                            'next_or_number'   => 'number',
                            'separator'        => ' ',
                            'pagelink'         => '%',
                            'echo'             => 1
                        );
                        wp_link_pages( $defaults );
                    endwhile; 
                    endif; ?>
                </article>
            </main>
            <?php get_sidebar('page-single'); ?>
        </div>
    </div>
<?php get_footer(); ?>
