#!/bin/bash

PARAM=$1
THEME_DIR=leather
DIR=$(pwd)

# Clean settings
GIT_DIR=.git
GIT_FILES=(.gitattributes .gitignore)

# Package settings
DIRS_TO_DELETE=(.git .sass-cache)
FILES_TO_DELETE=(.gitattributes .gitignore clean.sh style.css.map)

# Help text
HELP=$"
    WordPress theme cleaner shellscript
    usage:

    -h --help
        Prints this help message
    
    -c --clean
        Clean theme (remove git files)
    
    -p --package
        Create theme zip package (and remove unwanted files)
"

if [ -z "$PARAM" ]; then
    echo "$HELP";
    exit 0
else
    if [[ ( $PARAM = "-c" ) || ( $PARAM = "--clean" ) ]]; then
        rm -rf $GIT_DIR

        for f in ${GIT_FILES[@]}; do
            rm $TEMP_DIR/$THEME_DIR/$f
        done

        exit 0
    elif [[ ( $PARAM = "-p" ) || ( $PARAM = "--package" ) ]]; then
        TEMP_DIR=$(mktemp -d)

        cp -rf ../$THEME_DIR/ $TEMP_DIR/$THEME_DIR/

        for d in ${DIRS_TO_DELETE[@]}; do
            rm -rf $TEMP_DIR/$THEME_DIR/$d
        done

        for f in ${FILES_TO_DELETE[@]}; do
            rm $TEMP_DIR/$THEME_DIR/$f
        done

        cd $TEMP_DIR
        zip -r $THEME_DIR.zip leather
        cp -f $THEME_DIR.zip $DIR/$THEME_DIR.zip
        rm -rf $TEMP_DIR

        exit 0
    elif [[ ( $PARAM = "-h" ) || ( $PARAM = "--help" ) ]]; then
        echo "$HELP";
        exit 0
    else
        echo "    Non existing parameter!"
        echo "$HELP";
        exit 0
    fi

    exit 0
fi

exit 0