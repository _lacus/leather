<?php
/**
 * Front page template file
 *
 * PHP version 7
 *
 * @category   Page_Template
 * @package    WordPress
 * @subpackage Leather
 * @author     Varga László <varga.laszlo@protonmail.com>
 * @license    GNU General Public License v2 or later
 * @link       https://codex.wordpress.org/Creating_a_Static_Front_Page
 * @since      Leather 1.0
 */
 
get_header(); ?>
<div class="container">
    <div class="row">
        <main id="main" class="
            herd col 
            col-12 
            col-sm-8 
            col-md-9 
            col-lg-9">
            <h2 class="main_title sr-only"><?php _e('Posts', 'leather'); ?></h2>
            <?php if (have_posts() ) :
                while ( have_posts() ) : the_post();
                    get_template_part('components/post');
                endwhile;
                    get_template_part('components/pagination');
                else :
                endif; ?>
        </main>
        <?php get_sidebar('blog'); ?>
    </div>
</div>
<?php get_footer(); ?>