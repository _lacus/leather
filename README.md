![Leather](img/leather_logo.png)

# Leather

Contributors: Varga L�szl�  
Requires at least: WordPress 4.0  
Tested up to: WordPress 4.9  
Version: 2.1    
Tags: blog, custom-logo, custom-menu, custom-background, featured-images, left-sidebar, theme-options, translation-ready, two-columns

## Description

Leather theme is a starting theme for WordPress, and Bootstrap based websites.

## Git branches

- [Master - Stable](https://bitbucket.org/_lacus/leather/src/master/)
- [Develop - Latest](https://bitbucket.org/_lacus/leather/src/develop/)
- [Legacy - Bootstrap v3.x](https://bitbucket.org/_lacus/leather/src/legacy/)

## Installation
1. In command line go to `wp-conten/themes` folder, and clone theme:
  
```bash
  git clone git@bitbucket.org:_lacus/leather.git
```

2. In your admin panel, go to `Appearance -> Themes`.
3. On `Leather` theme Click on the `Activate` button to use it.

## Required plugins

* [Breadcrumb NavXT](https://wordpress.org/plugins/breadcrumb-navxt/)
* [Widget CSS Classes](https://wordpress.org/plugins/widget-css-classes/) 
* [WP Page Numbers](https://wordpress.org/plugins/wp-page-numbers/)

To install required plugins go to admin panel `Appearance -> Reqired Plugins` and install.

## Development

To recompile CSS via PostCSS, Autoprefixer:

```bash
npm install
npm run scss
```

To watch SCSS file changes, and compile

```bash
npm run scss:w
```

## Resources

Leather bundles the following third-party resources:

**Bootstrap**  
Copyright: (c) 2011-2016 Twitter, Inc., The Bootstrap Authors   
License: [MIT](https://github.com/twbs/bootstrap/blob/master/LICENSE)   
Version: 4.1.3
Source: [http://getbootstrap.com/](http://getbootstrap.com/)

**Font Awesome**  
Copyright: Dave Gandy  
License: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/), [SIL OFL 1.1](http://scripts.sil.org/OFL), [MIT](https://opensource.org/licenses/mit-license.html)  
Version: Font Awesome Free 5.0.6
Source: [http://fontawesome.com/](http://fontawesome.com)

**TGM-Plugin-Activation**  
Copyright: Thomas Griffin  
License: [GPL-2.0](https://opensource.org/licenses/GPL-2.0)    
Version: 2.6.1  
Source: [http://tgmpluginactivation.com/](http://tgmpluginactivation.com/)

**WP Bootstrap Navwalker**  
Copyright: Edward McIntyre  
License: [GPL-3.0](https://github.com/wp-bootstrap/wp-bootstrap-navwalker/blob/master/LICENSE.txt)    
Version: 4.0.1  
Source: [https://github.com/wp-bootstrap/wp-bootstrap-navwalker](https://github.com/wp-bootstrap/wp-bootstrap-navwalker)

**Open color**  
Copyright: (c) 2016 heeyeun  
License: [MIT](https://github.com/yeun/open-color/LICENSE)  
Version: 1.5.1  
Source: [https://yeun.github.io/open-color/](https://yeun.github.io/open-color/)

**mark.js**  
Copyright: (c) 2014-2017 Julian Motz  
License: [MIT license](https://github.com/julmot/mark.js/blob/master/LICENSE)    
Version: 8.4.0  
Source: [https://github.com/julmot/mark.js](https://github.com/julmot/mark.js)

## License

GPL-2.0

## Changelog

### 2.1
* Bugfixes
* Sass watch command
* Theme color settings
* Released on Oktober 9, 2018
### 2.0
* Bugfixes
* Compiling SCSS via node-sass, autoprefixer
* Bootstrap update to 4.x
* Bootstrap navwalker update to 4.x
* Font awsome update to 5.x 
* Theme language fixes
* Hungarian language file
* Released on Oktober 6, 2018
### 1.2
* Bugfixes
* Released on February 16, 2018
### 1.1
* Theme cleanup, bugfixes, page settings panel, facebook app id to settings panel.
* Released on July 2, 2017
### 1.0
* Released on July 1, 2017
